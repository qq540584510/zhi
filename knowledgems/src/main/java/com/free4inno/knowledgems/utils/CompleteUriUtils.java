package com.free4inno.knowledgems.utils;

import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;

/**
 * Author HUYUZHU.
 * Date 2020/10/21 17:15.
 */

@Slf4j
public class CompleteUriUtils {

    public static String getCompleteUri(HttpServletRequest request) {
        log.info("CompleteUriUtils" + "----in----" + "生成带有参数的完整Uri" + "----");
        String uri = request.getRequestURI();
        String param = request.getQueryString();
        String completeUri = "";
        if (param == null || param.equals("")) {
            completeUri = uri;
        } else {
            completeUri = uri + "?" + param;
        }
        log.info("CompleteUriUtils" + "----out----" + "返回completeUri" + "----");
        return completeUri;
    }
}
