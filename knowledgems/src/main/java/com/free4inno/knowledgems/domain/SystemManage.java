package com.free4inno.knowledgems.domain;

import lombok.Data;

import javax.persistence.*;

import java.sql.Timestamp;

/**
 * Author: ZhaoHaotian.
 * Date: 2021/04/24.
 */
@Entity
@Table(name = "system_manage")
@Data
public class SystemManage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "variable")
    private String variable; // 变量名（主键）

    @Column(name = "value")
    private String value; // 变量值

    @Column(name = "set_time")
    private Timestamp setTime; // 修改时间

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Timestamp getSetTime() {
        return setTime;
    }

    public void setSetTime(Timestamp setTime) {
        this.setTime = setTime;
    }

}
